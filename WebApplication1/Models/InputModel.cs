﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class InputModel
    {
        public int param1 { get; set; }

        public int param2 { get; set; }

        public ResultModel Calculate()
        {
            return new ResultModel { Sum = (param1 + param2) };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult InputData()
        {
            return View();
        }

        public IActionResult ResultData(InputModel data)
        {
            ResultModel model = data.Calculate();

            return View(model);
        }
        public IActionResult Graph()
        {
            ViewBag.x = new int[] { 10, 20, 30, 40, 50, 60 };
            ViewBag.y = new int[] { 15, 29, 45, 66, 88, 105 };
            ViewBag.y2 = new int[] { 19, 20, 21, 22, 23, 24 };

            return View();
        }
        public IActionResult MyPage()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
